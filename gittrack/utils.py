#    This file is a part of GitTrack
#
#    GitTrack is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GitTrack is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import re

__all__ = [
    'parse_args',
    'parse_repo'
]


def parse_args(command, quantity=None):
    try:
        args = command.split()[1:]
        if quantity is None:
            return args
        return args[:quantity]

    except IndexError:
        return -1


def parse_repo(repo):
    if re.match("^.*/.*$", repo):
        return repo.split("/")
    else:
        return None
