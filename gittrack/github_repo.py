#    This file is a part of GitTrack
#
#    GitTrack is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GitTrack is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


from github import Github
from github import GithubException
from gittrack.config_parser import get_config
from datetime import datetime

try:
    git = Github(login_or_token=get_config("github")["token"])
    print(git.get_rate_limit())
except GithubException:
    git = Github(login_or_token=get_config("github")["api_username"],
                 password=get_config("github")["api_password"])
    print(git.get_rate_limit())

__all__ = [
    'Repo'
]


class Repo:
    def __init__(self, user_name, repo_name):
        self.user = git.get_user(user_name)
        self.user_url = f"https://api.github.com/users/{user_name}"
        self.repo = self.user.get_repo(repo_name)
        self.repo_url = f"https://github.com/{self.repo.full_name}"
        self.name = self.repo.name

        self.info = f"Repository: *{self.repo.name.capitalize()}*\n" \
                    f"Maintainer: *{self.user.name}*\n\n" \
                    f"Description:\n{self.repo.description}\n\n" \
                    f"Last push: " \
                    f"{self.repo.pushed_at.strftime('%a %d %b %Y, %H:%M:%S')}"\
                    f"\n\n" \
                    f"How to clone:\n" \
                    f"```\n" \
                    f"git clone {self.repo_url}.git\n" \
                    f"```"

    def show_info(self):
        return self.info

    def fetch_tags(self):
        tags = []
        for tag in self.repo.get_tags():
            tags.append(tag)
        return tags

    def fetch_commits(self, since=None, until=None):
        if since is None:
            since = datetime(
                datetime.today().year,
                datetime.today().month-1,
                datetime.today().day,
                0, 0, 0
            )
            until = datetime.today()

        commits = []
        for commit in self.repo.get_commits(since=since, until=until):
            commits.append(commit)

        return commits
