#    This file is a part of GitTrack
#
#    GitTrack is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GitTrack is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import psycopg2
from gittrack.github_repo import Repo
from gittrack.config_parser import get_config

__all__ = [
    'create_repository_tables',
    'drop_repository_tables',
    'get_table_name',
    'insert_repository_info',
    'check_repo_in_database',
    'get_all_repositories',
    'get_repo_data',
    'types',
    'change_repo_schedule',
    'get_repo_schedule',
    'get_latest_commit',
    'get_latest_tag',
    'insert_commit',
    'insert_tag'
]

database = psycopg2.connect(
    database=get_config("database")["database"],
    user=get_config("database")["user"],
    password=get_config("database")["password"],
    host=get_config("database")["host"],
    port=get_config("database")["port"])


def create_repository_tables(repo: Repo):
    repo_name = f"{repo.user.login.replace('-', '_')}_" \
        f"{repo.name.replace('-', '_')}"

    with database.cursor() as cur:
        cur.execute(f"CREATE TABLE {repo_name}_info("
                    f"id int primary key, "
                    f"name text not null, "
                    f"maintainer text not null, "
                    f"description text not null, "
                    f"clone_link text not null, "
                    f"last_push timestamp);")

        cur.execute(f"CREATE TABLE {repo_name}_commits("
                    f"hash varchar(40) primary key,"
                    f"message text,"
                    f"commiter_name text not null,"
                    f"commiter_email text not null,"
                    f"created_at timestamp not null,"
                    f"is_pull_request bool not null);")

        cur.execute(f"CREATE TABLE {repo_name}_pull_requests("
                    f"id serial primary key,"
                    f"github_id varchar(40) not null,"
                    f"hash varchar(40) not null,"
                    f"title text,"
                    f"author text not null,"
                    f"url text not null,"
                    f"created_at timestamp not null);")

        cur.execute(f"CREATE TABLE {repo_name}_tags("
                    f"hash varchar(40) primary key,"
                    f"name text not null,"
                    f"tarball_url text,"
                    f"zipball_url text,"
                    f"created_at timestamp not null);")

        cur.execute("INSERT INTO repositories "
                    f"VALUES('{repo_name}', "
                    f"'{repo.user.login}', "
                    f"'{repo.name}', 10);")
    database.commit()
    insert_repository_info(repo)


def get_table_name(repo: Repo):
    cur = database.cursor()
    query = "SELECT repo_table_name " \
        "FROM repositories WHERE " \
        "maintainer=%s AND " \
        "repo_name=%s;"
    cur.execute(query,
                (repo.user.login, repo.name))
    database.commit()
    return cur.fetchall()[0][0]


def insert_repository_info(repo: Repo):
    repo_data = [
        repo.repo.id,
        repo.name,
        repo.user.login,
        repo.repo.description,
        f"{repo.repo_url}.git",
        repo.repo.pushed_at
    ]

    with database.cursor() as cur:
        cur.execute(f"INSERT INTO {get_table_name(repo)}_info "
                    "VALUES(%s, %s, %s, %s, %s, %s)",
                    repo_data)
    database.commit()


def drop_repository_tables(repo: Repo):
    repo_name = f"{repo.user.login.replace('-', '_')}_" \
        f"{repo.name.replace('-', '_')}"

    with database.cursor() as cur:
        cur.execute(f"DROP TABLE {repo_name}_info")
        cur.execute(f"DROP TABLE {repo_name}_commits")
        cur.execute(f"DROP TABLE {repo_name}_pull_requests")
        cur.execute(f"DROP TABLE {repo_name}_tags")
        cur.execute("DELETE FROM repositories WHERE "
                    f"repo_table_name='{repo_name}'")
    database.commit()


def check_repo_in_database(repo: Repo):
    with database.cursor() as cur:
        cur.execute(
            "SELECT * FROM repositories WHERE maintainer=%s AND repo_name=%s",
            (repo.user.login, repo.name)
        )

        if cur.fetchone():
            return True
        else:
            return False
    database.commit()


def get_all_repositories():
    cur = database.cursor()

    cur.execute("SELECT * FROM repositories")

    repos = []

    for table in cur.fetchall():
        repos.append(Repo(table[1], table[2]))

    database.commit()
    return repos


types = {
    "info": 0,
    "commits": 1,
    "tags": 2,
    "pull_requests": 3
}


def get_repo_data(repo: Repo, type=types["info"]):
    cur = database.cursor()

    table = get_table_name(repo)

    if type == 0:
        query = f"SELECT * from {table}_info;"
        cur.execute(query)
        database.commit()
        return cur.fetchone()
    elif type == 1:
        pass
    elif type == 2:
        pass
    elif type == 3:
        pass
    else:
        return -1


def change_repo_schedule(repo: Repo, time):
    table = get_table_name(repo)
    with database.cursor() as cur:
        cur.execute(
            "UPDATE repositories "
            "SET schedule=%(int)s "
            f"WHERE repo_table_name='{table}';",
            {"int": time}
        )
    database.commit()


def get_repo_schedule(repo: Repo):
    table = get_table_name(repo)
    with database.cursor() as cur:
        cur.execute(
            "SELECT schedule "
            "FROM repositories "
            "WHERE repo_table_name = %s",
            (table,)
        )
        database.commit()
        return cur.fetchone()[0]


def get_latest_commit(repo: Repo):
    table = get_table_name(repo)

    query = \
        "SELECT MAX(created_at) " \
        f"FROM {table}_commits " \
        "WHERE is_pull_request = false"

    with database.cursor() as cur:
        cur.execute(query)

        database.commit()
        return cur.fetchone()[0]


def get_latest_tag(repo: Repo):
    table = get_table_name(repo)

    query = \
        "SELECT * " \
        f"FROM {table}_tags " \
        "WHERE created_at = (SELECT MAX(created_at) " \
        f"FROM {table}_tags)"

    with database.cursor() as cur:
        cur.execute(query)

        database.commit()
        return cur.fetchone()


def insert_commit(repo: Repo, data):
    table = get_table_name(repo)

    query = \
        f"INSERT INTO {table}_commits " \
        "VALUES (%s, %s, %s, %s, %s, %s)"
    try:
        with database.cursor() as cur:
            cur.execute(query, data)
            database.commit()
        return True
    except psycopg2.DatabaseError:
        database.rollback()
        return False


def insert_tag(repo: Repo, data):
    table = get_table_name(repo)

    query = \
        f"INSERT INTO {table}_tags " \
        "VALUES (%s, %s, %s, %s, %s)"

    try:
        with database.cursor() as cur:
            cur.execute(query, data)
            database.commit()
        return True
    except psycopg2.DatabaseError:
        database.rollback()
        return False
