#    This file is a part of GitTrack
#
#    GitTrack is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GitTrack is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


from configparser import ConfigParser
import argparse

__all__ = [
    'get_config'
]

config = ConfigParser()

parser = argparse.ArgumentParser(__name__)
parser.add_argument('-c', '--config', type=str)

args = vars(parser.parse_args())
cfg_path = args.get('config')


def get_config(section):
    config.read(cfg_path)
    return config[section.upper()]
