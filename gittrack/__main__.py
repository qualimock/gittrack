from gittrack.telegram_bot import bot
from gittrack.updater import schedule_updates
import threading

if __name__ == "__main__":
    tbot_thread = threading.Thread(target=bot.infinity_polling)
    schedule_thread = threading.Thread(target=schedule_updates)

    tbot_thread.start()
    schedule_thread.start()
