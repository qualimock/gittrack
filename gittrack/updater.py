#    This file is a part of GitTrack
#
#    GitTrack is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GitTrack is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


from gittrack.github_repo import Repo
from gittrack.telegram_bot import bot
from gittrack import utils
from gittrack import database
from datetime import datetime
import github
import schedule
import time

__all__ = [
    'restart_schedule'
]

sched = schedule


def get_commit_data(commit: github.Commit.Commit):
    return (
        commit.sha,
        commit.commit.message,
        commit.commit.committer.name,
        commit.commit.committer.email,
        commit.commit.committer.date,
        False
    )


def get_tag_data(tag: github.Tag):
    return (
        tag.commit.sha,
        tag.name,
        tag.tarball_url,
        tag.zipball_url,
        tag.commit.commit.committer.date
    )


def update_commits(repo: Repo):
    latest_commit = database.get_latest_commit(repo)
    commits = []

    if latest_commit is None:
        commits = repo.fetch_commits()
    else:
        commits = repo.fetch_commits(latest_commit, datetime.today())[1:]

    commits_data = []

    for commit in commits:
        commit_data = get_commit_data(commit)

        print("Get commit data: ")
        print(commit_data)

        commits_data.append(commit_data)
        database.insert_commit(repo, commit_data)

    return commits_data


def update_tags(repo: Repo):
    latest_tag = database.get_latest_tag(repo)
    tags = repo.fetch_tags()

    tags_data = []

    for tag in tags:
        tag_data = get_tag_data(tag)

        print("Get tag data: ")
        print(tag_data)

        if tag_data == latest_tag:
            break

        tags_data.append(tag_data)

    for tag_data in tags_data:
        database.insert_tag(repo, tag_data)

    return tags_data


def update_repo_data(repo: Repo):
    new_commits = update_commits(repo)
    new_tags = update_tags(repo)

    for commit in new_commits:
        bot.new_commit(repo, commit)

    for tag in new_tags:
        bot.new_tag(repo, tag)


def schedule_updates():
    def get_schedules(repos):
        schedules = []
        for repo in repos:
            schedules.append(
                database.get_repo_schedule(repo)
            )
        return schedules

    schedules = dict(
        zip(
            database.get_all_repositories(),
            get_schedules(database.get_all_repositories())
        )
    )

    for repo in schedules.keys():
        sched.every(schedules[repo]).minutes.do(
            update_repo_data, repo=repo
        ).tag(f"{repo.user.login}/{repo.name}")

    while True:
        sched.run_pending()
        time.sleep(1)


def restart_schedule(repo: Repo):
    repo_name = f"{repo.user.name}/{repo.name}"

    sched.clear(repo_name)

    sched.every(database.get_repo_schedule(repo)).minutes.do(
        update_repo_data, repo=repo
    ).tag(repo_name)


@bot.message_handler(commands=["schedule",
                               "sched"])
def schedule_repository(message):
    repo = utils.parse_repo(utils.parse_args(message.text, 1)[0])
    try:
        repo = Repo(repo[0], repo[1])
    except (github.GithubException, TypeError):
        bot.send_message(message.chat.id, "Repository not found")
        return

    if not database.check_repo_in_database(repo):
        bot.send_message(message.chat.id, "Repository exists, but not tracked")
        return
    try:
        schedule = int(utils.parse_args(message.text, 2)[1])
    except ValueError:
        bot.send_message(message.chat.id, "Invalid argument")
        return

    database.change_repo_schedule(repo, schedule)
    restart_schedule(repo)
    bot.send_message(message.chat.id,
                     "<strong>Repo:</strong> "
                     f"{repo.user.login}/{repo.name}\n\n"
                     f"Updates every {schedule}min")
