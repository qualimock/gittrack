#    This file is a part of GitTrack
#
#    GitTrack is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GitTrack is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


from telebot import TeleBot
from gittrack.config_parser import get_config
from gittrack import database
from gittrack.github_repo import Repo
from gittrack.utils import parse_args, parse_repo
import github

bot = TeleBot(token=get_config("telegram")["token"],
              parse_mode="html")
chat_id = get_config("telegram")["chat_id"]

__all__ = [
    'bot',
    'new_tag',
    'new_commit'
]


@bot.message_handler(commands=['track', 'untrack'])
def track_commands_handler(message):
    repos = parse_args(message.text)

    for repo in repos:
        try:
            if "github.com" in repo:
                repo = Repo(repo.split("/")[3],
                            repo.split("/")[4])
            elif repo := parse_repo(repo):
                repo = Repo(repo[0], repo[1])
            else:
                raise Exception()

        except (github.GithubException, Exception):
            bot.send_message(message.chat.id, "Repository not found")
            continue

        repo_message = "<strong>Repo:</strong> " \
            f"{repo.user.login}/{repo.name}\n\n"

        if message.text.split()[0] == "/untrack":
            bot.send_message(message.chat.id,
                             repo_message + "Really <strong>untrack</strong>?")

            msg = f"Print <strong>{repo.user.login}/{repo.name}</strong>\n" \
                  "If you really want to untrack the repository."

            bot.register_next_step_handler(
                bot.send_message(message.chat.id, msg),
                untrack, repo
            )
            return

        if database.create_repository_tables(repo) == -1:

            bot.send_message(message.chat.id,
                             repo_message + "<strong>Already tracked</strong>")
            continue

        bot.send_message(message.from_user.id,
                         repo_message + "<strong>Begin tracking</strong>")


def untrack(message, repo):
    repo_message = "<strong>Repo:</strong> " \
        f"{repo.user.login}/{repo.name}\n\n"

    if message.text == f"{repo.user.login}/{repo.name}":
        database.drop_repository_tables(repo)
        bot.send_message(message.chat.id,
                         repo_message + "<strong>Untracked</strong>")
    else:
        bot.send_message(message.chat.id,
                         repo_message + "Still <strong>tracked</strong>")


@bot.message_handler(commands=["repos",
                               "repositories",
                               "repos_list",
                               "repo_list"])
def show_repositories(message):
    repos = database.get_all_repositories()

    msg = "<strong>Tracked repositories:</strong>\n\n"

    for repo in repos:
        msg += f"{repo.user.login}/{repo.name}\n"

    bot.send_message(message.chat.id, msg)


@bot.message_handler(commands=["show_info",
                               "info",
                               "repo_info"])
def show_repository_info(message):
    repo = parse_repo(parse_args(message.text, 1)[0])
    try:
        repo = Repo(repo[0], repo[1])
    except (github.GithubException, TypeError):
        bot.send_message(message.chat.id, "Repository not found")
        return

    if not database.check_repo_in_database(repo):
        bot.send_message(message.chat.id, "Repository exists, but not tracked")
        return

    data = database.get_repo_data(repo, database.types["info"])

    info = \
        f"<strong>Repo:</strong> {data[2]}/{data[1]} (id: {data[0]})\n\n" \
        f"<strong>Descripiton:</strong>\n{data[3]}\n\n" \
        f"<strong>Clone link:</strong> {data[4]}\n\n" \
        "<strong>Last push: </strong>" \
        f"{data[5].strftime('%a %d %b %Y %H:%M:%S')}"

    bot.send_message(
        chat_id=message.chat.id,
        text=info,
        disable_web_page_preview=True
    )


@bot.message_handler(commands=["schedules",
                               "scheds",
                               "get_schedules"])
def show_schedules(message):
    repos = database.get_all_repositories()
    message = "<strong>Repository update schedule</strong>\n\n"

    for repo in repos:
        message += \
            f"{repo.user.login}/{repo.name}: " \
            f"{database.get_repo_schedule(repo)}min\n"

    bot.send_message(chat_id, message)


def new_commit(repo: Repo, commit):
    message = \
        "<strong>Repo:</strong> " \
        f"{repo.user.login}/{repo.name}\n\n" \
        "<strong>NEW COMMIT</strong>\n\n" \
        f"<strong>Hash: </strong>{commit[0]}\n" \
        f"<strong>Committer: </strong>{commit[2]}\n" \
        f"<strong>Committer email: </strong>{commit[3]}\n" \
        "<strong>Commited at </strong>" \
        f"{commit[4].strftime('%a %d %b %Y %H:%M:%S')}\n\n" \
        f"<strong>Message: </strong>{commit[1]}"
    bot.send_message(chat_id, message)


def new_tag(repo: Repo, tag):
    tarball = tag[2].split("/")[-1]
    zipball = tag[3].split("/")[-1]

    message = \
        "<strong>Repo:</strong> " \
        f"{repo.user.login}/{repo.name}\n\n" \
        "<strong>NEW TAG</strong>\n\n" \
        f"<strong>Hash: </strong>{tag[0]}\n" \
        f"<strong>Name: </strong>{tag[1]}\n" \
        "<strong>Tarball: </strong>" \
        f"<a href=\"{tag[2]}\">{tarball}</a>\n" \
        "<strong>Zipball: </strong>" \
        f"<a href=\"{tag[3]}\">{zipball}</a>\n" \
        "<strong>Created at </strong>" \
        f"{tag[4].strftime('%a %d %b %Y %H:%M:%S')}\n\n"
    bot.send_message(chat_id, message)
