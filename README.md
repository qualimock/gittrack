# GitTrack

This is a simple GitHub tracker telegram bot 
> bot'll be soon, I promise :D

## Configure
Edit `config.conf`

```ini
[GITHUB]
# you can use access token to increase amount of queries to Github servers
token = some-github-accesstoken
# if you don't have access token you can use your username and password
api_username = bestusername777
api_password = StRonGpassWORd0

[TELEGRAM]
token = <there is no need in this yet>

```
## Use
Just run it with
```bash
$ python3 main.py
```
