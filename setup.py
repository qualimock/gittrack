#    GitTrack is a Telegram bot that tracks Github repositories.
#    Copyright (C) 2022  Quali Mock <qualimock@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from setuptools import setup, find_packages


def find_requirements():
    with open('requirements.txt') as file:
        req = file.readlines()
    return req


setup(
    name='GitTrack',
    version='0.1',
    url='https://gitlab.com/qualimock/gittrack',
    license='GPLv3',
    author='Quali Mock',
    author_email='qualimock@gmail.com',
    description='Telegram bot that tracks Github repositories',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    install_requires=find_requirements(),
    packages=find_packages(),
    long_description=open('README.md').read(),
    test_suite='pytest'
)
